;;; mu4e-query-helper.el --- Aid in creating a search query for mu4e  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2021 Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: mail, convenience
;; Modified: 2021-07-01
;; URL: https://gitlab.com/andersjohansson/mu4e-query-helper
;; Package-Requires: ((emacs "25.1") (mu4e "1.4"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides completion for constructing a mu4e search query

;;; Code:

(require 'mu4e)
(require 'mailcap)
(require 'mail-parse)
(require 'subr-x)
(require 'cl-lib)

;; From mu-find manual:

;; Here is the full table of the search fields and their
;; abbreviations:

;; cc,c            Cc (carbon-copy) recipient(s)
;; bcc,h           Bcc (blind-carbon-copy) recipient(s)
;; from,f          Message sender
;; to,t            To: recipient(s)
;; subject,s       Message subject
;; body,b          Message body
;; maildir,m       Maildir
;; msgid,i         Message-ID
;; prio,p          Message priority ('low', 'normal' or 'high')
;; flag,g          Message Flags
;; date,d          Date-Range
;; size,z          Message size
;; embed,e         Search inside embedded text parts (messages, attachments)
;; file,j          Attachment filename
;; mime,y          MIME-type of one or more message parts
;; tag,x           Tags for the message (X-Label and/or X-Keywords)
;; list,v          Mailing list (e.g. the List-Id value)

;; There  are  also  the special fields contact, which matches all
;; contact-fields (from, to, cc and bcc), and recip, which matches
;; all recipient-fields (to, cc and bcc).


(defconst mu4e-query-helper-search-keywords
  '(("Contact (from, to, cc, bcc)" . "contact")
    ("Message sender (from)" . "from")
    ("To: recipient(s)" . "to")
    ("Cc (carbon-copy) recipient(s)" . "cc")
    ("Bcc (blind-carbon-copy) recipient(s)" . "bcc")
    ("Recip (to, cc, bcc)" . "recip")
    ("Message subject" . "subject")
    ("Message body" . "body")
    ("Maildir" . "maildir")
    ("Message-ID" . "msgid")
    ("Message priority high" . "prio:high")
    ("Message priority normal" . "prio:normal")
    ("Message priority low" . "prio:low")
    ("Flag:Draft Message" . "flag:draft")
    ("Flag:Flagged" . "flag:flagged")
    ("Flag:New message (in new/ Maildir)" . "flag:new")
    ("Flag:Passed ('Handled')" . "flag:passed")
    ("Flag:Replied" . "flag:replied")
    ("Flag:Seen" . "flag:seen")
    ("Flag:Marked for deletion" . "flag:trashed")
    ("Flag:Has attachment" . "flag:attach")
    ("Flag:Signed message" . "flag:signed")
    ("Flag:Encrypted message" . "flag:encrypted")
    ("Flag:Mailing-list message" . "flag:list")
    ("Date-Range" . "date")
    ("Message size" . "size")
    ("Search inside embedded text parts (messages, attachments)" . "embed")
    ("Attachment filename" . "file")
    ("MIME-type of one or more message parts" . "mime")
    ("Tags for the message (X-Label and/or X-Keywords)" . "tag")
    ("Mailing list (e.g. the List-Id value)" . "list")))


(defvar mu4e-query-helper-history-text nil "History for searches of text in messages: body, fulltext, embedded parts.")
(defvar mu4e-query-helper-history-contact nil)
(defvar mu4e-query-helper-history-subject nil)
(defvar mu4e-query-helper-history-body nil)
(defvar mu4e-query-helper-history-maildir nil)
(defvar mu4e-query-helper-history-flag nil)
(defvar mu4e-query-helper-history-date nil)
(defvar mu4e-query-helper-history-size nil)
(defvar mu4e-query-helper-history-file nil)
(defvar mu4e-query-helper-history-mime nil)
(defvar mu4e-query-helper-history-tag nil)
(defvar mu4e-query-helper-history-list nil)

(defvar mu4e-query-helper--current-kw nil)

(defun mu4e-query-helper-search (&optional full)
  "Construct and execute a mu4e search query with completion aid."
  (interactive "P")
  (let ((mu4e-headers-full-search
         (or full mu4e-headers-full-search)))
    (mu4e-headers-search (mu4e-query-helper--generate-query) nil t)))

(defun mu4e-query-helper-search-edit (&optional full)
  "Edit previous query, with possible additions"
  (interactive)
  (let ((mu4e-headers-full-search
         (or full mu4e-headers-full-search)))
    (mu4e-headers-search
     (concat mu4e~headers-last-query
             (mu4e-query-helper--generate-query mu4e~headers-last-query))
     nil t)))

(defvar helm-comp-read-map) ;; silence byte-compiler

(defun mu4e-query-helper--generate-query (&optional extra-prompt)
  "Repeatedly prompt for query terms with completion."
  (cl-loop with cont = t
           with keywords = (mapcar #'cdr mu4e-query-helper-search-keywords)
           while cont
           concat
           (let* ((prompt
                   (concat
                    "Keyword or full-text: "
                    (when extra-prompt
                      (format "[CURRENT: %s] " extra-prompt))))
                  (kw
                   (if (bound-and-true-p helm-mode)
                       (helm
                        :prompt prompt
                        :sources (list
                                  (helm-build-sync-source "Search keyword"
                                    :candidates mu4e-query-helper-search-keywords
                                    :keymap helm-comp-read-map)
                                  (helm-build-dummy-source "Fulltext"
                                    :keymap helm-comp-read-map
                                    :history mu4e-query-helper-history-text)))
                     (completing-read
                      prompt
                      keywords
                      nil nil nil
                      'mu4e-query-helper-history-text))))
             (cond
              ((or (null kw) ; quit
                   (string-blank-p kw))
               (setq cont nil))
              ((string-match-p ":" kw) ; already complete: flag or prio
               (setq extra-prompt (concat extra-prompt kw " "))
               (concat kw " "))
              ((member kw keywords) ; keyword that needs input
               (progn
                 (setq mu4e-query-helper--current-kw kw)
                 (when-let ((this (mu4e-query-helper--keyword-complete kw)))
                   (setq extra-prompt (concat extra-prompt this))
                   (setq mu4e-query-helper--current-kw nil)
                   this)))
              (t (concat kw " ")) ; full-text search
              ))))

(defun mu4e-query-helper--keyword-complete (keyword)
  "Prompt for a search string with possible completion for all mu4e query keywords."
  (let ((kws (concat keyword ": ")))
    (pcase keyword
      ((or "from" "to" "contact" "recip" "cc" "bcc")
       (unless (hash-table-p mu4e~contacts-hash)
         (mu4e-query-helper-request-contacts-wait))
       (mu4e-query-helper--helm-or-cr
        kws
        mu4e~contacts-hash
        :history 'mu4e-query-helper-history-contact
        :default (mu4e-query-helper--from-or-to)
        :candtrans-helm #'hash-table-keys
        ;; :candtrans-helm #'mu4e-query-helper--sort-contact-list
        ))
      ("mime" (mu4e-query-helper--helm-or-cr
               kws
               (mailcap-mime-types)
               :history 'mu4e-query-helper-history-mime))
      ("maildir" (mu4e-query-helper--helm-or-cr
                  kws
                  (delete-dups
                   (append (mapcar #'car mu4e-maildir-shortcuts)
                           (mu4e-get-maildirs)))
                  :history mu4e-query-helper-history-maildir))
      ("date" (mu4e-query-helper--add-kw
               (mu4e-query-helper--read-string
                "date [YYYYDDMM..YYYYDDMM or 2w..now]: "
                'mu4e-query-helper-history-date)))
      ("size" (mu4e-query-helper--add-kw
               (mu4e-query-helper--read-string
                "size [10K..2M]: "
                'mu4e-query-helper-history-size)))
      ((or "body" "embed")
       (mu4e-query-helper--add-kw (mu4e-query-helper--read-string kws 'mu4e-query-helper-history-text)))
      ("list" (mu4e-query-helper--helm-or-cr
               kws (mu4e-query-helper--get-mailing-lists)
               :candtrans-cr (apply-partially #'mapcar #'cdr)))
      ;; no special handling:
      ((pred (lambda (x) (member x (mapcar #'cdr mu4e-query-helper-search-keywords))))
       (mu4e-query-helper--add-kw (mu4e-query-helper--read-string kws (intern (concat "mu4e-query-helper-history-" keyword)))))
      ;; keyword not defined here
      (_ (mu4e-query-helper--add-kw (read-string kws))))))

;;;; Helper functions

(cl-defun mu4e-query-helper--helm-or-cr (prompt candidates &key history default candtrans-helm candtrans-cr)
  (if (bound-and-true-p helm-mode)
      (helm :sources (helm-build-sync-source prompt
                       :candidates (if candtrans-helm (funcall candtrans-helm candidates) candidates)
                       :action #'mu4e-query-helper--helm-concatenate-cands
                       :history history)
            :preselect default
            :prompt prompt)
    (mu4e-query-helper--rep-quote-kw
     (completing-read prompt
                      (if candtrans-cr (funcall candtrans-cr candidates) candidates)
                      nil nil nil history default))))

(defun mu4e-query-helper--helm-concatenate-cands (c)
  (let ((cands (helm-marked-candidates)))
    (if (< 1 (safe-length cands))
        (concat "("
                (mapconcat #'mu4e-query-helper--rep-quote-kw
                           (helm-marked-candidates)
                           (if (and (< 1 (safe-length cands))
                                    (y-or-n-p "Concatenate with OR? (else AND)"))
                               "OR "
                             "AND "))
                ") ")
      (mu4e-query-helper--rep-quote-kw c))))

(defun mu4e-query-helper--add-kw (x)
  (concat mu4e-query-helper--current-kw ":" x " "))

(defun mu4e-query-helper--read-string (prompt histvar)
  "Read string, prompting with PROMPT and with history stored in HISTVAR"
  (let ((ans (mu4e-query-helper--maybe-quote (read-string prompt nil histvar))))
    (add-to-history histvar ans)
    ans))

(defun mu4e-query-helper--maybe-quote (string)
  "Add quotes if string contains space"
  (if (string-match-p " " string)
      (concat "\"" string "\"")
    string))

(defun mu4e-query-helper--maybe-rep (string)
  (pcase mu4e-query-helper--current-kw
    ((or "from" "to" "contact" "recip" "cc" "bcc")
     (car (mail-header-parse-address string)))
    (_ string)))

(defun mu4e-query-helper--rep-quote-kw (s)
  (mu4e-query-helper--add-kw
   (mu4e-query-helper--maybe-quote
    (mu4e-query-helper--maybe-rep s))))

(defun mu4e-query-helper--from-or-to ()
  "When the from address for message at point is one of the the user's addresses,
as per `mu4e-user-mail-address-list', return the to address.
Otherwise, return the from address."
  (when-let ((msg (mu4e-message-at-point t)))
    (let ((addr (cdr-safe (car-safe (mu4e-message-field msg :from)))))
      (if (mu4e-user-mail-address-p addr)
          (mu4e-query-helper--first-contact (mu4e-message-field msg :to))
        (mu4e-query-helper--first-contact (mu4e-message-field msg :from))))))

(defun mu4e-query-helper--first-contact (contacts)
  (let ((name (caar contacts)) (email (cdar contacts)))
    (cond
     ((and name email)
      (concat name " <" email ">"))
     (email email)
     (t nil))))

;; Fetching contacts synchronously
(defvar mu4e-query-helper--notfetched t "Used for checking fetch status")
(defun mu4e-query-helper--signal-fetch (&rest _ignore)
  (setq mu4e-query-helper--notfetched nil))

(defun mu4e-query-helper-request-contacts-wait ()
  (unwind-protect
      (let ((mu4e-compose-complete-addresses t))
        (advice-add 'mu4e~update-contacts :after #'mu4e-query-helper--signal-fetch)
        (mu4e~request-contacts-maybe)
        (while mu4e-query-helper--notfetched (sleep-for 0.1)))
    (advice-remove 'mu4e~update-contacts #'mu4e-query-helper--signal-fetch)
    (setq mu4e-query-helper--notfetched t)))


(defun mu4e-query-helper--get-mailing-lists ()
  (cl-loop for l in (append mu4e-user-mailing-lists
                            mu4e~mailing-lists)
           collect (cons (concat (cdr l) " (" (car l) ")") (car l))))

(provide 'mu4e-query-helper)
;;; mu4e-query-helper.el ends here

;; Local Variables:
;; no-byte-compile: t
;; End:
